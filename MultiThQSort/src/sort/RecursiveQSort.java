package sort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import tsv.TsvObject;
import tsv.TsvProcessor;

/**
 * This class contains methods to run quicksort algorithm
 * 
 * @author dasha
 * 
 * @param <T>
 *            Comparable object
 */
public class RecursiveQSort<T extends Comparable<? super T>> implements
		Runnable {
	public static final Random RND = new Random();
	public static final int THRESHOLD = 50;
	private ArrayList<T> array;
	private int low;
	private int high;
	
	public RecursiveQSort(ArrayList<T> a, int l, int h) {
		this.array = a;
		this.low = l;
		this.high = h;

	}

	/**
	 * This method swaps two elements in a list
	 * 
	 */
	private static <T extends Comparable<? super T>> void swap(
			ArrayList<T> list, int i, int j) {
		T tmp = list.get(i);
		list.set(i, list.get(j));
		list.set(j, tmp);
	}

	/**
	 * 
	 * quicksort partition with random pivot
	 */
	private static <T extends Comparable<? super T>> int partition(
			ArrayList<T> list, int lo, int hi) {
		int index = RND.nextInt(hi - lo + 1) + lo;
		// int index = lo;
		T pivot = list.get(index);
		// System.out.println("pivot " + index);
		// System.out.println("value " + pivot);
		swap(list, lo, index); // now pivot is the first
		index = lo;
		int i = index + 1;
		for (int j = i; j <= hi; j++) {
			if (list.get(j).compareTo(pivot) <= 0) {
				swap(list, i, j);
				i = i + 1;
			}
		}
		swap(list, index, i - 1);
		return i - 1;
	}
	
	/**
	 * 
	 * quicksort without multithreading
	 */
	public static <T extends Comparable<? super T>> void quicksort(
			ArrayList<T> v) {
		quicksort(v, 0, v.size() - 1);
	}
	/**
	 * 
	 * recursive implementation of quicksort
	 */
	private static <T extends Comparable<? super T>> void quicksort(
			ArrayList<T> v, int lo, int hi) {
		if (hi - lo < THRESHOLD)
			insertionsort(v, lo, hi);
		else {
			int p = partition(v, lo, hi);
			quicksort(v, lo, p - 1);
			quicksort(v, p + 1, hi);
		}
	}
	/**
	 * 
	 * insertion sort algorithm to be used when list is smaller than THRESHOLD
	 */
	public static <T extends Comparable<? super T>> void insertionsort(
			ArrayList<T> v, int lo, int hi) {
		for (int i = lo; i <= hi; i++) {
			int j = i;
			T elem = v.get(i);
			while ((j > lo) && (v.get(j - 1).compareTo(elem) > 0)) {
				v.set(j, v.get(j - 1));
				j--;
			}
			v.set(j, elem);
		}
	}

	@Override
	public void run() {
		// System.out.println("sorting " + low + " " + high);
		quicksort(array, low, high);
	}

	public static <T extends Comparable<? super T>> void merge(ArrayList<T> a,
			int lo1, int lo2, int hi2) {
		int i = lo1;
		int j = lo2;
		ArrayList<T> res = new ArrayList<T>();
		while (i < lo2 && j < hi2 + 1) {
			if (a.get(i).compareTo(a.get(j)) < 0) {
				res.add(a.get(i));
				i++;
			} else {
				res.add(a.get(j));
				j++;
			}
		}
		for (int m = i; m < lo2; m++) {
			res.add(a.get(m));
		}
		for (int l = j; l < hi2 + 1; l++) {
			res.add(a.get(l));
		}
		for (int k = lo1; k < hi2 + 1; k++) {
			a.set(k, res.get(k - lo1));
		}
	}
	/**
	 * multithreaded quicksort
	 * @param a list to be sorted
	 * @param numThreads number of threads to be used
	 */
	public static <T extends Comparable<? super T>> void sort(ArrayList<T> a,
			int numThreads) {
		ExecutorService executor = Executors.newFixedThreadPool(numThreads);
		int splits = numThreads;
		ArrayList<Integer> splitPoints = new ArrayList<Integer>();
		int size = a.size() / splits;
		int current = 0;
		for (int i = 0; i < splits; ++i) {
			splitPoints.add(current);
			int next = current + size - 1;
			if (i == splits - 1)
				next = a.size() - 1;
			// System.out.println("current " + current + " next " + next);
			executor.execute(new RecursiveQSort<T>(a, current, next));
			current = next + 1;
		}
		executor.shutdown();

		while (!executor.isTerminated()) {

		}
		while (splitPoints.size() > 1) {
			merge(a, splitPoints.get(splitPoints.size() - 2),
					splitPoints.get(splitPoints.size() - 1), a.size() - 1);
			splitPoints.remove(splitPoints.size() - 1);
		}
	}
/**
 * 
 * a method to check if list is sorted
 */
	public static <T extends Comparable<? super T>> boolean isSorted(
			ArrayList<T> a) {
		for (int i = 1; i < a.size(); i++) {
			if (a.get(i).compareTo(a.get(i - 1)) < 0) {
				return false;
			}
		}
		return true;
	}

}