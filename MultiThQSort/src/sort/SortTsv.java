package sort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tsv.TsvObject;
import tsv.TsvProcessor;
/**
 * This class contains method to sort a tsv by key using threads
 * @author dasha
 *
 */
public class SortTsv {
	/**
	 * 
	 * @param inputPath path to tsv to be sorted
	 * @param key sort by this key
	 * @param numberOfThreads number of threads to be used
	 * @param outputPath path to file where output is to be written
	 */
	public static void sortTsv(String inputPath, int key, int numberOfThreads, String outputPath){
		try {
			ArrayList<TsvObject> tsvObjects = TsvProcessor.readTsv(inputPath, key);
			long start = System.currentTimeMillis();
			RecursiveQSort.sort(tsvObjects, numberOfThreads);
	    	long end = System.currentTimeMillis();
	    	System.out.println("Time: " + (end - start)/1000.0);
	    	//System.out.println(RecursiveQSort.isSorted(tsvObjects));
			TsvProcessor.printTsv(tsvObjects, outputPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){		
		for (int i = 0; i < 10; ++i)
    	sortTsv("test-jacopo.tsv", 0, 4, "out");
    }
}
