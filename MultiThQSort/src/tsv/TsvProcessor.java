package tsv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * this class contains methods to process a tsv file
 * @author dasha
 *
 */
public class TsvProcessor {
	public static ArrayList<TsvObject> readTsv(String path, int key) throws IOException {
		ArrayList<TsvObject> tsvObjects = new ArrayList<TsvObject>();
//		List<String[]> lines = new ArrayList<String[]>();
		BufferedReader br = new BufferedReader(new FileReader(path));
		int numVal = 0;
		int line = 0;
		while (br.ready()) {
			line++;
			String currentLine = br.readLine();//.replaceAll("\\t\\t", "\\t");
			String[] values = currentLine.split("\\t");
			if (numVal == 0)
				numVal = values.length;
			else if (numVal != values.length) {
			/*	System.err.println("Wrong number of values in line " + line
						+ "; expected " + numVal + ", found " + values.length);*/
			}
			tsvObjects.add(new TsvObject(values, key));
		}
		return tsvObjects;
	}
	
	public static void printTsv(ArrayList<TsvObject> tsvObjects){
		for(TsvObject obj: tsvObjects){
			for(String value: obj.values){
				System.out.print(value + "\t");
			}
			System.out.print("\n");
		}
	}
	
	
	public static void printTsv(List<TsvObject> tsvObjects, String output) throws IOException{
		FileWriter writer = new FileWriter(output);
		for(TsvObject obj: tsvObjects){
			for(int i = 0; i < obj.values.length-1; i++){
				writer.write(obj.values[i] + "\t");
			}			
			writer.write(obj.values[obj.values.length-1] + "\n");
		}
		writer.close();
	}
}
