package tsv;
/**
 * this class represents a tsv file
 * @author dasha
 *
 */
public class TsvObject implements Comparable<TsvObject> {
	protected String[] values;
	protected int key;
	
	public TsvObject(String[] values, int key){
		if(key > values.length - 1 || key < 0){
			System.err.println("Wrong key");
		}
		this.values = values;
		this.key = key;
	}
	
	@Override
	public int compareTo(TsvObject tsv) {
		return this.values[key].compareTo(tsv.values[key]);
	}

}
